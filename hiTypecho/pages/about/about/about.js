/* HiTypecho-微信小程序版Typecho
   使用教程：www.hiai.top
   有任何使用问题请联系作者邮箱 goodsking@163.com
*/
const app = getApp();
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    ColorList: app.globalData.ColorList,
  },
  onLoad: function () { },
  pageBack() {
    wx.navigateBack({
      delta: 1
    });
  }
});